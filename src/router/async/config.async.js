import routerMap from './router.map'
import {parseRoutes} from '@/utils/routerUtil'

// 异步路由配置
const routesConfig = [
  'login',
  'root',
  'signup',
  'editdeal',
  {
    router: 'exp404',
    path: '*',
    name: '404'
  },
  {
    router: 'exp403',
    path: '/403',
    name: '403'
  },
  {
    router: 'signup',
    path: '/signup',
    name: 'signup'
  },



  
]

const options = {
  routes: parseRoutes(routesConfig, routerMap)
}

export default options

/*
{"code":0,
"data":{"user":{"name":"test60"},
"token":"eyJhbGciOiJSUzI1NiIsImtpZCI6Ii1pcTgzeU43VmhyU0cwcXp6NUZUN0pkZm9kVTFkLW5nRjZTYU9yRHh3LW8iLCJ0eXAiOiJKV1QifQ.eyJhdWQiOlsibG9jYWxob3N0OjgwODEiXSwiYXV0aF90aW1lIjoxNjQwODMyMzcwLCJleHAiOjE2NDA4MzU5NzAsImlhdCI6MTY0MDgzMjM3MCwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwic3ViIjoiNiJ9.TrgXd-4QG8FzLOyUfpKl8sta1PG4gNahc0GeYVzWTrK2VJU2B4lO43u_Chxsvg4Yl_Ep3hCLu24io-T-BL6hkBeJyklzTKWLioE2gsoMTCo-cU08-dk74EY9L13AgkIGP5bgniD3BtX4yOxsYoJpbabr-3DsxI0jFmAmDMZf1Gti78_O9HOjFHEnlaSC8QXirAPb0r8baYHnWu9e7PLDZwlzFYR1_6NxUKI4lZq6jLxxzqh94G1NTGtPRrFd-Jjzvx1aD6VkCTQkqrX0xmraHVPNoxVc-83azAQLMk5HlyoCajE6bsPANI1UlfPfINDUIHlzjeQBjsUoPI81ODw2uw",
"roles":[""],
"permissions":["admin"],
"data":[{"router":"root","children":[{"router":"brodishManager","children":["bdmDash","bdmCarrierDispatchCurrentDay","bdmWorkerDispatchCurrentDay","bdmCarrierDispatchPast","bdmWorkerDispatchPast","bdmCarrierUpcoming","bdmWorkerUpcoming","bdmCarrierCompliance","bdmWorkerCompliance","bdmRecommendation","bdmCommunication","bdmNotification"]}]}]}}
*/