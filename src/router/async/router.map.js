//import { component } from 'vue/types/umd'


// 视图组件
const view = {
  tabs: () => import('@/layouts/tabs'),
  blank: () => import('@/layouts/BlankView'),
  page: () => import('@/layouts/PageView')
}

// 路由组件注册
const routerMap = {
  login: {
    authority: '*',
    path: '/login',
    component: () => import('@/pages/login')
  },

  editUpdate:{
    name: "editUpdate",
   component: ()=> import('../../pages/moderator-m/Discovery/update'), 
   //props: true },
  },
  signup: {
   authority: '*',
    path: '/signup',
    name: 'signup',
    component:() => import ('../../pages/signup/Signup'),
    //redirect: '/login',
  },

  root: {
    path: '/',
    name: 'Homepage',
    component: view.tabs,
    redirect: '/login',
  },
  exp403: {
    authority: '*',
    name: 'exp403',
    path: '403',
    component: () => import('@/pages/exception/403')
  },
  exp404: {
    name: 'exp404',
    path: '404',
    component: () => import('@/pages/exception/404')
  },

  dashboard: {
    name: 'Dashboard',
    component: view.blank
  },

  workplace: {
    name: 'workplace',
    component: () => import('@/pages/dashboard/workplace')
  },

  analysis: {
    name: 'analysis',
    component: () => import('@/pages/dashboard/analysis')
  },

  su: {
    name: 'su',
    component: view.blank,
  },
  suDash: {
  name: "suDash",
  //component: () => import('@/pages/dashboard/workplace')

 component: () => import ('@/pages/suviews/suDash')
},

  fixedCreate: {
    name: "fixedCreate",
    component: () => import('../../pages/suviews/fixedCreate/fixedCreate')
  },

  userCreate: {
    name: "userCreate",
    component: () => import('../../pages/suviews/UserCreate/userCreate')
  },

  allLocation: {
    name: "allLocation",
    component : view.blank
  },

  newRecommendations: {
    name: "newRecommendations",
    component : view.blank
  },

  //moderator routes registering
//acounting
  Accounting: {
    name: 'Accounting',
    component: view.blank,
  },
  allAccounting: {
    name: "allAccounting",
    component: () => import('../../pages/moderator-m/Accounting/accountinghome')
  },



  //closing routes
  Inclosing: {
    name: 'Inclosing',
    component: view.blank,
  },
  allInclosing: {
    name: "allInclosing",
    component:() => import('../../pages/moderator-m/Closing/inclosinghome')

  },

//discovery routes
Discovery: {
  name: 'Discovery',
  component: view.blank,
},
allDeals: {
  name: "allDeals",
  component:() => import('../../pages/moderator-m/Discovery/alldeals/index')

},

producerEndpoint : {
  name: "producerEndpoint",
  component:() => import('../../pages/moderator-m/Discovery/producerendpoint')

},



//staging
Staging:{
  name:"Staging",
 component: view.blank,
},

allStaged: {
  name: "allstaged",
  component:() => import('../../pages/moderator-m/Staging/allstaged')

},
adminShowingRoom: {
  name: "adminShowingRoom",
  component:() => import('../../pages/moderator-m/Staging/adminshowingroom')

},


//seller
Seller:{
  name:"Seller",
 component: view.blank,
},
sellerActive: {
  name: "sellerActive",
  component:() => import('../../pages/seller/selleractive')
},

sellerProducer : {
  name : "sellerProducer",
  component:() => import('../../pages/seller/sellerproducer')
},

// sellerHistory: {
//   name : "sellerHistory",
//   component:() => import('../../pages/seller/sellerhistory')
// },

// sellerViewUpdateDeal: {
//   name: "sellerViewUpdateDeal",
//   component:() => import('../../pages/seller/sellerviewupdatedeal')
// }


//bidding
Bidding:{
  name:"Bidding",
 component: view.blank,
},
adminLiveRoom: {
  name: "adminliveroom",
  component:() => import('../../pages/moderator-m/Biddables/adminliveroom')
},

// sellerProducer : {
//   name : "sellerProducer",
//   component:() => import('../../pages/seller/sellerproducer')
// },
  

//buyer

Buyer:{
  name:"Buyer",
 component: view.blank,
},
buyerActive: {
  
  name: "buyerActive",
  component:() => import('../../pages/buyer/buyeractive')
},

buyerShowingRoom: {
  name: "buyerShowingRoom",
  component:() => import('../../pages/buyer/buyershowingroom')
},
buyerLiveRoom: {
  name : "buyerLiveroom",
  component:() => import('../../pages/buyer/buyerliveroom')
},




  
}
export default routerMap

