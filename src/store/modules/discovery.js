/* eslint-disable no-unused-vars */
//import { values } from "core-js/core/array";
//import { CREATEONESTAGED } from "../../services/api"
import { GetAllDeals, CreateOneStaged, GetallStaged, ViewRoomManager } from "../../services/discovery"
export default {
    namespaced: true,
    state: {
      columns: [],
     data:[],
     allstaged: [],
     onedetailstaged: [],

     //allroommanager: [],
     created: {},
     selectedKeys:[],
     selectedName: "",
     selectedRule: "",
     inviewmodal: {},
     indeletemodal: {},
     StagedCreateOK: false,
     roomfocuskey:"ROOM1",
     roomfocuslist:[],
     
    },

    getters: {

        alldeals(state){
            return state.data
        },

        lengthSelected: state => state.selectedKeys.length,

        getLocal: (state) => (id) => {

            return state.data.find(thing => thing.id === id)
          },

          showstagedcreatedsuccess: (state) => {
            return state.StagedCreateOK
          },


        selected(state) {
            return state.created
        },
        canshowselected(state) {
 var len = Object.keys(state.created).length
            if (len > 0) {
                return true
            }else{
                return false
            }
        },
        onedetailstaged(state) {
            return state.onedetailstaged
        },
        allstaged(state){
            return state.allstaged
        },

        allroommanager(state){
            return state.roomfocuslist
        }



    },
      
    mutations: {
      setData (state, data) {
          state.data = data
        // state.user = user
        // localStorage.setItem(process.env.VUE_APP_USER_KEY, JSON.stringify(user))
     
    },


    setCreated(state, data) {
        state.created = data
    },
    // removeCreated(state, data) {
    //     state.created =
    // },
    setAllstaged(state, data) {
        state.allstaged = data
    },
    setOneStaged(state, data) {
        state.onedetailstaged = data
    },
    setallroommanager(state, data){
        state.allroommanager = data
    },
    setViewModal(state, data) {
        state.inviewmodal = data
    },
    setDeletModal(state, data) {
        state.indeletemodal = data
    },

    setSelectedName(state, data) {
        state.selectedName = data
    },
    setSelectedRule(state, data) {
        state.selectedRule = data
    },

    addkeytoSelected(state, keys) {
        state.StagedCreateOK = false
        state.selectedKeys = keys
        
    },
    setRoomFocusKey(state, key) {
        state.roomfocuskey = key
    },
    setRoomList(state, data) {
        console.log("setting room list")
        state.roomfocuslist = data
    }


    // removeKeytoSelected(state, key) {
    //     state.selectedKeys.remove(key)
    // }
    //   setPermissions(state, permissions) {
    //     state.permissions = permissions
    //     localStorage.setItem(process.env.VUE_APP_PERMISSIONS_KEY, JSON.stringify(permissions))
    //   },
    //   setRoles(state, roles) {
    //     state.roles = roles
    //     localStorage.setItem(process.env.VUE_APP_ROLES_KEY, JSON.stringify(roles))
    //   },
    //   setRoutesConfig(state, routesConfig) {
    //     state.routesConfig = routesConfig
    //     localStorage.setItem(process.env.VUE_APP_ROUTES_KEY, JSON.stringify(routesConfig))
    //   }
    },

    actions: {
        getData (context) {
          const data = [];
            GetAllDeals().then((res) =>{
                console.log(res.data.Data)
              
                data.push.apply(data,res.data.Data)
                
            }).catch((err)=>{
                console.log(err)
            })
    

        context.commit('setData', data)
        },

        getAllStaged (context) {
            const data = [];
              GetallStaged().then((res) =>{
                  console.log(res.data.Data)
                
                  data.push.apply(data,res.data.Data)
                  
              }).catch((err)=>{
                  console.log(err)
              })
      
  
          context.commit('setAllstaged', data)
          },
        SaveSelected({state,context}) {
            var form = {}
            form.keys = state.selectedKeys
            form.rule = state.selectedRule
            form.name = state.selectedName
            console.log(form)
            CreateOneStaged(form).then((res) =>{
                state.StagedCreateOK = true
            }).catch((err)=>{
                console.log(err)
            })
        },
        
        GetRoomDetail(context){
                var data = []
            ViewRoomManager(context.state.roomfocuskey).then((res)=>{
                data = res.data.Data
                console.log(data)
                context.commit('setRoomList', data)

            }).catch((err)=>{
                console.log(err)
            })

        }




    },
  }
  