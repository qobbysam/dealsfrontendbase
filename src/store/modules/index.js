import account from './account'
import setting from './setting'
import discovery from './discovery'

export default {account, setting, discovery}