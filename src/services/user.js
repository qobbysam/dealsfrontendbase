 import {LOGIN, ROUTES, USER_INFO, REGISTER, LOCALSIGNUP} from '@/services/api'
import { GET_ALL_GROUP,ADD_GROUP,GET_ONE_GROUP } from './api'
import {request, METHOD, removeAuthorization} from '@/utils/request'

/**
 * 登录服务
 * @param name 账户名
 * @param password 账户密码
 * @returns {Promise<AxiosResponse<T>>}
 */

//  const getUserinfo = function(response_){
//   // this.logging = false
//      console.log("doing getuserinfo")
//      console.log(response_.data)
 
//      let res = {}
//      res.token = ""

//      res.token = response_.data.token
//    //  res.data.result.id_token = response_.data.token
//      console.log(res)
//     //  response_.then(function(res){
//     //    console.log("response is loging")
//     //    console.log(res)
//     //  }).catch(function(err) {
//     //    console.log(err)
//     //  })

//     console.log("calling getInfo")
 
//    return  getInfo(res)
 
//  }
 
export async function login(name, password) {
  // //let pass = true;
  // let  login_request = request(LOGIN,  METHOD.POST,{
  //   email: name,
  //   password: password
  
  // }).then(function(response){
  //   // if (response.status === 307) {
  //   //   // message.error('403 response received on onrejected resp403')
 
  //   //   console.log("doing interception :2")
  //   //   console.log(response)

  //   console.log(response)
  //   return getUserinfo(response)
    
  //    // message.warning('login again token is missing')
  // }).catch(function(err){
  //   console.log("printing error 78" , err)
  //   console.log (login_request)

  //   return login_request
  // })

  let response = await request(LOGIN, METHOD.POST, {
    email: name,
    password: password
  })

  if (response.error){
    this.$message.error('UserName and password is Incorrect');
  }

  return response

  
}

export async function signup(email, password, password_confirm, name) {

  console.log("doing signup")

  return request(REGISTER, METHOD.POST, {
    email: email,
    password: password,
    password_confirm: password_confirm,
    name: name
  })

  
}

export async function signupLocal(token, username, groupid) {
console.log("doing localsignup")

  return request(LOCALSIGNUP, METHOD.POST, {
    token: token,
    username: username,
    groupid: groupid,
   // name: name
  })

  
}



export async function getInfo(res) {
console.log("async get info called")
  return request(USER_INFO, METHOD.GET, {
    token: res.data.token,
  }) 
}

export async function getRoutesConfig() {
  return request(ROUTES, METHOD.GET)
}



export async function getAllGroup() {
  return request(GET_ALL_GROUP, METHOD.GET)
}

export async function addGroup(name, permissions) {
  return request(ADD_GROUP, METHOD.POST,{
    name: name,
    permissions: permissions
  })
}

export async function getOneGroup(group) {

  return request(GET_ONE_GROUP, METHOD.GET, {

    id: group
  })
}



export function logout() {
  localStorage.removeItem(process.env.VUE_APP_ROUTES_KEY)
  localStorage.removeItem(process.env.VUE_APP_PERMISSIONS_KEY)
  localStorage.removeItem(process.env.VUE_APP_ROLES_KEY)
  removeAuthorization()
}
export default {
  login,
  logout,
  getRoutesConfig,

  getAllGroup,
  addGroup,
  getOneGroup
}
