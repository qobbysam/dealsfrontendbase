//import BASE_URL from './api'
import {request, METHOD} from '@/utils/request'
import { ALLDEALS, CREATEONEDEAL, CREATEONESTAGED, GETALLSTAGED, GETONEDEAL, GETONESTAGEDDETAIL, UPDATEONEDEAL, VIEWROOMBUYER, VIEWROOMMANAGER } from './api'

//discovery routes


export async function GetAllDeals() {

    return request(ALLDEALS, METHOD.GET)
}


export async function CreateOneDeal(form) {

    return request(CREATEONEDEAL, METHOD.POST, {
        carname: form.carname,
        carsellerprice: form.carsellerprice,
        carprofitprice: form.carprofitprice,
        caryear: form.caryear,
        carmake: form.carmake,
        carvin: form.carvin
    })

}

export async function UpdateOneDeal(form) {

    return request(UPDATEONEDEAL, METHOD.POST, {
        carid : form.carid,
        carname: form.carname,
        carsellerprice: form.carsellerprice,
        carprofitprice: form.carprofitprice,
        caryear: form.caryear,
        carmake: form.carmake,
        carvin: form.carvin
    })

}


export async function GetOneDeal(id) {
    return request(GETONEDEAL, METHOD.GET, {id:id})
} 


export async function GetOneStagedDetail(id) {
    return request(GETONESTAGEDDETAIL, METHOD.GET, {id:id})
} 

export async function GetallStaged(id) {
    return request(GETALLSTAGED, METHOD.GET, {id:id})
} 

export async function ViewRoomBuyer(id) {
    return request(VIEWROOMBUYER, METHOD.GET, {roomid:id})
} 

export async function ViewRoomManager(id) {
    return request(VIEWROOMMANAGER, METHOD.GET, {roomid:id})
} 


export async function CreateOneStaged(form) {

    return request(CREATEONESTAGED, METHOD.POST, {
        keys: form.keys,
        name: form.name,
        rule: form.rule
    })

}


export async function DeleteOneDeal() {

}



export async function UploadOneImg () {}


export async function DeleteOneImg() {}

export async function UploadOnePdf() {}


export async function DeleteOnePdf() {}

export async function UpdateImgOrder() {}