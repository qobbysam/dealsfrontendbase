//跨域代理前缀
// const API_PROXY_PREFIX='/api'
// const BASE_URL = process.env.NODE_ENV === 'production' ? process.env.VUE_APP_API_BASE_URL : API_PROXY_PREFIX
//const BASE_URL = process.env.VUE_APP_API_BASE_URL
//const LOGIN_BASE = process.env.VUE_APP_API_LOGIN_BASE


// //const BASE_URL = ``
// const LOGIN_BASE = `http://localhost:3000/auth`



// const BASE_URL = `http://localhost:10010/api`
// const LOGIN_BASE =`http://localhost:3000/auth`
//IMG_BASE = `http://127.0.0.1:10012/files/img?path=`

// const MODERATORWORKERID = "PX4EeEndXPUiKqXQ59n4Lm"
// const MODERATORMANAGERID = "iPwbPdD4KEznHdppmzL2TV"
// const BUYERID = "dEqLm3pKhyKoznyuzVPcgE"
// const SELLERID = "jH54rbKmeuuoMfLZWb79KK"

const BASE_URL = `https://dealstagingapplication.ga/api`
const LOGIN_BASE =`https://auth.dealstagingapplication.ga/auth`
const IMG_BASE = `https://files.dealstagingapplication.ga/files/img?path=`

const MODERATORWORKERID = "3R4vEL5Mm3ey8S94dTcfXm"
const MODERATORMANAGERID = "nhsoR8jkXW5gw6Nx5asGLU"
const BUYERID = "9f9euKeVe7EqLQEETMXVN4"
const SELLERID = "sQEQxtEfyXBCEUQjFScWwU"

module.exports = {
  IMG_BASE,
  MODERATORMANAGERID,
  MODERATORWORKERID,
  BUYERID,
  SELLERID,

  LOGIN: `${LOGIN_BASE}/login`,
  REGISTER:`${LOGIN_BASE}/register`,
  LOGOUT : `${LOGIN_BASE}/logout`,

  //ROUTES: `${BASE_URL}/routes`,
  
  USER_INFO: `${BASE_URL}/auth/account/client/get-info`,
  LOCALSIGNUP: `${BASE_URL}/auth/account/client/signup`,
  
  //GOODS: `${BASE_URL}/goods`,
  //GOODS_COLUMNS: `${BASE_URL}/columns`,
//fixed create
  ADD_GROUP : `${BASE_URL}/account/add-group`,
  GET_ALL_GROUP: `${BASE_URL}/account/get-all-group`,
  GET_ONE_GROUP: `${BASE_URL}/account/get-one-group`,


  //discovery routes
  ALLDEALS: `${BASE_URL}/discover/discover/all-deals`,
  CREATEONEDEAL: `${BASE_URL}/discover/discover/create-one-deal`,
 //UPDATEONEDEAL: `${BASE_URL}/discover/discover/create-one-deal`,
  GETONEDEAL: `${BASE_URL}/discover/discover/one-deal`,
  DELETEONEDEAL: `${BASE_URL}/discover/discover/delete-one-deal`,
  UPDATEONEDEAL: `${BASE_URL}/discover/discover/update-one-deal`,
  UPLOADONEIMG: `${BASE_URL}/discover/discover/upload-one-img`,
  DELETEONEIMG: `${BASE_URL}/discover/discover/delete-one-img`,
  UPLOADONEPDF: `${BASE_URL}/discover/discover/upload-one-pdf`,
  DELETEONEPDF: `${BASE_URL} /discover/discover/delete-one-pdf`,
  UPDATEIMGORDER: `${BASE_URL}/discover/discover/update-deal-img-order`,


  //staged

  GETONESTAGEDDETAIL : `${BASE_URL}/staged/staged/get-staged-detail`,
  CREATEONESTAGED : `${BASE_URL}/staged/staged/create-staged`,
  GETALLSTAGED  : `${BASE_URL}/staged/staged/all-staged`,

  VIEWROOMMANAGER: `${BASE_URL}/showing/showing/all-room-manager`,
  VIEWROOMBUYER: `${BASE_URL}/showing/showing/all-room-buyer`,
}
